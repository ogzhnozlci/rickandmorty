import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom'
import './App.css';

import Header from './containers/Header'
import Home from './containers/Home'
import Detail from './containers/Detail'

class App extends Component {

  render() {
    return (
        <Router>
            <div className="App">
                <Header/>
                <Route exact path='/' component={Home}></Route>
                <Route exact path='/detail/:id' component={Detail}></Route>
            </div>
        </Router>

    );
  }
}

const mapStateToProps = state => ({
  ...state
})

const mapDispatchToProps = dispatch => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
