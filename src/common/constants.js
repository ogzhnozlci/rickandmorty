const API_BASE = "https://rickandmortyapi.com/api/";

export const APIs = {
    "characters": API_BASE + "character/",
    "locations": API_BASE + "location/",
    "episodes": API_BASE + "episode/"
}
