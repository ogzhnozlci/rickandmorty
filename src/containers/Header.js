import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = {
    progress: {
        position: 'absolute',
        right: 10,
    }
}

class Home extends Component {
    render() {

        return (
            <AppBar position="sticky">
                <Toolbar>
                    <Typography variant="h6" color="inherit">
                        RickAndMorty
                    </Typography>
                    {
                        this.props.loading &&
                        <CircularProgress style={styles.progress} color={"secondary"}/>
                    }

                </Toolbar>
            </AppBar>
        );
    }
}


const mapStateToProps = state => {
    return {
        loading: state.commonReducer.result
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators({}, dispatch);
};


export default connect(mapStateToProps, mapDispatchToProps)(Home);
