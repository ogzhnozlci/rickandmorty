import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import InfList from '../components/InfList'

import { fetchList, fetchNextList } from "../actions/homeAction";

class Home extends Component {
    constructor (props) {
        super(props);
        this.state = {
            listData:[]
        };
        this.getMoreData = this.getMoreData.bind(this)
    }

    componentDidMount() {
        this.props.fetchList();
    }

    componentDidUpdate(prevProp, prevState) {
        if (this.props.listData !== prevProp.listData) {
            this.updateListData(this.props.listData);
        }
    }

    updateListData(data){
        this.setState(prevState => {
            return {listData: [...prevState.listData || [], ...data]}
        })
    }

    getMoreData(){
        this.props.fetchNextList();
    }

    render() {

        return (
            <div className="Home">
                <InfList
                    endOfScrollFunc={this.getMoreData}
                    listData={this.state.listData}
                    loading={this.props.loading}
                    link={"/detail/"}
                    linkParam={"id"}
                />

            </div>
        );
    }
}


const mapStateToProps = state => {
    return {
        listData: state.homeReducer.result,
        loading: state.commonReducer.result
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchList, fetchNextList }, dispatch);
};


export default connect(mapStateToProps, mapDispatchToProps)(Home);
