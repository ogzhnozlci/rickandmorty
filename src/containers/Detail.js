import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';


import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import BackButton from "../components/BackButton"

import { fetchDetail } from "../actions/detailAction";

const styles = {
    card: {
        maxWidth: 600,
        margin:"30px auto"
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
        backgroundSize: "contain"
    },
    listItem: {
        textAlign:"center"
    }
};

class Detail extends Component {
    componentDidMount() {
        const id = this.props.match.params.id
        this.props.fetchDetail(id);
    }

    componentDidUpdate(prevProps, prevState) {

    }

    render() {
        if (!this.props.detailData) {
            return false;
        }

        const data = this.props.detailData;

        return (
            <div className="Detail">
                <BackButton/>
                <Card style={styles.card}>
                    <CardHeader
                        title={data.name}
                        subheader={data.origin.name}
                    />
                    <CardMedia
                        style={styles.media}
                        image={data.image}
                        title={data.name}
                    />
                    <CardContent>
                        <Typography variant="h5">Last Episodes</Typography>
                        <List>
                            {data.lastEpisodes.map((episode,i) => {
                                return (
                                    <ListItem style={styles.listItem} key={i}>
                                        <ListItemText primary={episode.episode + " - " + episode.name} secondary={episode.date} />
                                    </ListItem>
                                )
                            })}

                        </List>
                    </CardContent>
                </Card>
            </div>
        );
    }
}


const mapStateToProps = state => {
    return {
        detailData: state.detailReducer.result,
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchDetail }, dispatch);
};


export default connect(mapStateToProps, mapDispatchToProps)(Detail);
