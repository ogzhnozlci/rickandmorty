export default (state = {}, action) => {
    switch (action.type) {
        case 'FETCH_LIST':
            return {
                result: action.payload
            }
        default:
            return state
    }
}
