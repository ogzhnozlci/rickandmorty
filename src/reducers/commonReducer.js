export default (state = {}, action) => {
    switch (action.type) {
        case 'LOADING':
            return {
                result: action.payload
            }
        default:
            return state
    }
}
