export default (state = {}, action) => {
    switch (action.type) {
        case 'FETCH_DETAIL':
            return {
                result: action.payload
            }
        default:
            return state
    }
}
