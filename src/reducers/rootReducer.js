import { combineReducers } from 'redux';
import homeReducer from './homeReducer';
import commonReducer from './commonReducer';
import detailReducer from './detailReducer';

export default combineReducers({
    homeReducer,
    detailReducer,
    commonReducer
});
