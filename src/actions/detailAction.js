import axios from 'axios';

import { APIs } from "../common/constants";

const _episodeLimit = 5;

const getDetail = async (id) => {
 const URL = APIs.characters + id
 let detail = await axios.get(URL);

 return detail.data;
}

const prepareEpisodesReq = (episodes) => {
 if (episodes.length > _episodeLimit) {
  episodes = episodes.slice(_episodeLimit*-1);
 }

 let reqArr = episodes.map(link => {
  return axios.get(link);
 })

 return reqArr;
}

const prepareEpisodesData = (episodeRes) => {
 let episodeArr = episodeRes.map((episode)=>{
   let episodeData = episode.data;
   return {
    name:episodeData.name,
    date:episodeData.air_date,
    episode:episodeData.episode
   }
 })

 return episodeArr;
}

export const fetchDetail = (id) => async (dispatch) => {
 dispatch({
  type: 'LOADING',
  payload: true
 })

 let data = await getDetail(id);

 const episodeReqs = prepareEpisodesReq(data.episode)
 const episodeRes = await Promise.all(episodeReqs)
 const lastEpisodes = prepareEpisodesData(episodeRes);
 data.lastEpisodes = lastEpisodes;

 dispatch({
  type: 'FETCH_DETAIL',
  payload: data
 })

 dispatch({
  type: 'LOADING',
  payload: false
 })
}
