import axios from 'axios';

import { APIs } from "../common/constants";

let _reqInfo;

const prepareData = function(resp) {
 const {data} = resp;
 _reqInfo = data.info;
 const list = data.results;

 return list;
}

export const fetchList = () => dispatch => {
 dispatch({
  type: 'LOADING',
  payload: true
 })

 axios.get(APIs.characters).then((resp)=>{
   const list = prepareData(resp);

  dispatch({
   type: 'FETCH_LIST',
   payload: list
  })

  dispatch({
   type: 'LOADING',
   payload: false
  })
 })
}

export const fetchNextList = () => dispatch => {
 if (!_reqInfo || !_reqInfo.next) {
  dispatch({
   type: 'FETCH_LIST',
   payload: []
  })
  return false;
 }

 dispatch({
  type: 'LOADING',
  payload: true
 })

 axios.get(_reqInfo.next).then((resp)=>{
  const list = prepareData(resp);

  dispatch({
   type: 'FETCH_LIST',
   payload: list
  })

  dispatch({
   type: 'LOADING',
   payload: false
  })
 })
}
