import React, { Component } from 'react';
import {
    Link
} from 'react-router-dom'

import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';

const scrollGap = 500;

const styles = {
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: "white",
    },
    gridList: {
        maxWidth: 750
    },
    icon: {
        color: 'rgba(255, 255, 255)',
        fontWeight: 'bold',
        padding: 15
    },
}

class InfList extends Component {

    addScrollEvent(){
        window.addEventListener("scroll", ()=>this.handleScroll(),false);
    }

    removeScrollEvent(){
        window.removeEventListener('scroll',()=>this.handleScroll(), false);
    }

    handleScroll() {
        if ( (window.innerHeight + window.scrollY) >= (document.body.offsetHeight - scrollGap) && !this.props.loading ) {
            this.props.endOfScrollFunc();
        }
    };

    componentDidMount() {
        this.addScrollEvent();
    }

    componentDidUpdate(prevProp, prevState) {

    }

    componentWillUnmount() {
        this.removeScrollEvent();
    }

    render() {
        const colNum = window.innerWidth < 900 ? 2 : 3;
        return (
            <div className="InfList" style={styles.root}>
                <GridList cellHeight={300} cols={colNum} style={styles.gridList}>
                    {this.props.listData.map((tile,i) => (
                            <GridListTile key={i}>
                                    <img src={tile.image} alt={tile.name} />
                                    <GridListTileBar
                                        title={tile.name}
                                        actionIcon={
                                            <Link style={styles.icon} to={this.props.link + tile[this.props.linkParam]}>
                                                Detail
                                            </Link>
                                        }
                                    />
                            </GridListTile>
                    ))}
                </GridList>
            </div>
        );
    }
}

export default InfList;
