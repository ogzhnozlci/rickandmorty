import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';

import Button from '@material-ui/core/Button';

const styles = {
    button: {
        position:"absolute",
        left: 15,
        top: 75
    },
};

class BackButton extends Component {
    goBack(){
        this.props.history.goBack()
    }

    render() {
        return (
            <Button style={styles.button} variant="contained" color="primary" onClick={()=> this.goBack()}>
                Go Back
            </Button>
        );
    }
}

export default withRouter(BackButton);
